;; New year, new .emacs  DMM 2014
;; NB: Much of this thanks to Magnar from EmacsRocks
(cd "~")
(if (fboundp 'menu-bar-mode)   (menu-bar-mode -1))
(if (fboundp 'tool-bar-mode)   (tool-bar-mode -1))
(if (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))
(setq inhibit-startup-message t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Default color scheme is really hard on the eyes.  Fix that first
;; thing
(set-face-background 'default   "grey80")
(set-face-foreground 'default   "black")
(set-face-background 'mode-line "grey65")          ; mode-line too...
(set-face-foreground 'mode-line "black")
(set-face-background 'mode-line-inactive "grey60")
(set-face-foreground 'mode-line-inactive "black")


;; Set up load path
(setq site-lisp-dir (expand-file-name "site-lisp" user-emacs-directory))
(add-to-list 'load-path user-emacs-directory)
(add-to-list 'load-path site-lisp-dir)

(require 'setup-package)   ; Setup and install default packages

;; Determine osx/win/linux and set specific key bindings to regularize
;; the interface, plus window appearance
(require 'dmm-defaults)   ; Fix emacs boneheadedness (better-defaults.el based)
(require 'dmm-platform)   ; Setup dmm/platform-is-mac?, etc
(require 'dmm-appearance) ; Setup colors, general window appearance

;; Local settings for used packages
(require 'setup-smex)      ; smart M-x
(require 'setup-flycheck)  ; on-the-fly syntax check
(require 'setup-yasnippet) ; snippets
(require 'setup-dired)
(require 'setup-p4)
(require 'setup-cc-mode)

;; =============================================================================
;; Tiny stuff
;; =============================================================================
(require 'visible-mark)
(visible-mark-mode t)
(set-face-background 'visible-mark-face "lightgreen")

(global-auto-revert-mode t)

;; Ace jump,
(require 'ace-jump-mode)
(require 'ace-jump-buffer)
(global-set-key (kbd "M-m") 'ace-jump-mode)
(global-set-key (kbd "M-n") 'ace-jump-buffer)
(global-set-key (kbd "M-i") 'back-to-indentation)

;; Desktop save/restore and emacs reset
(global-set-key (kbd "C-S-M-<backspace>") 'desktop-clear)
(global-set-key (kbd "C-M-_")             'desktop-save-in-desktop-dir)
(global-set-key (kbd "C-M-+")             'desktop-revert)

;; 
(global-set-key (kbd "C-c C-z s") 'dmm/sort-includes)


;; Expand region
(require 'expand-region)
(global-set-key (kbd "C-=") 'er/expand-region)

;; Emacs server
(require 'server)
(unless (server-running-p) (server-start))

;; =============================================================================
;; =============================================================================

;; =============================================================================
;; Things to look into
;; =============================================================================

; cpputils-cmake
; find-file- at-point/in-project
; diminish
; mode line darker than background for splits
; figure out magit
; webjump
; flyspell?
; f (package)
; disaster
; desktop-registry
; smartparens
; projectile
; eshell
; gist
; uniqify


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Use RAD build tool from compile command

;; Local settings and stuffs
(require 'codehelper)

(setq *setenv-command*
      "d:\\dev\\rad\\bin\\dmoore\\build_envs\\work.bat")

(defvar *rad-base-directory* "d:/dev/rad/"
  "Base directory of the rad repository on this machine")

(defun make-rad-subdirectory (subdir)
  "Makes a subdirectory of the RAD root for this machine"
  (concat *rad-base-directory* subdir))

(defun make-granny-subdirectory (subdir)
  "Makes a subdirectory of the Granny root for this machine"
  (make-rad-subdirectory (concat "granny/" subdir)))

(setq-default compile-command "t")

(defmacro make-rad-compile (fn-name dir &optional command)
  (let ((cmd (or command "t")))
    `(defun ,fn-name (&optional arg)
       (interactive)
       (compile (concat ,*setenv-command* " && "
                        "cd " ,dir
                        " && " ,cmd " "
                        (or arg ""))))))

(defmacro bind-rad-compile (key command)
  `(let ()
     (global-set-key (kbd ,key) (lambda () (interactive) (,command)))
     (global-set-key (kbd ,(concat "S-" key)) (lambda () (interactive) (,command "-a")))
     (global-set-key (kbd ,(concat "M-" key)) (lambda () (interactive) (,command "-q")))
     (global-set-key (kbd ,(concat "M-S-" key)) (lambda () (interactive) (,command "-a -q")))
     (global-set-key (kbd ,(concat "C-" key)) (lambda () (interactive) (,command "-r")))
     (global-set-key (kbd ,(concat "M-C-" key)) (lambda () (interactive) (,command "-r -q")))
     (global-set-key (kbd ,(concat "C-S-" key)) (lambda () (interactive) (,command "-a -r")))
     (global-set-key (kbd ,(concat "M-C-S-" key)) (lambda () (interactive) (,command "-a -r -q")))))

(make-rad-compile rad-compile-all           (make-rad-subdirectory    "granny"))
(make-rad-compile rad-compile-rt            (make-granny-subdirectory "rt"))
(make-rad-compile rad-compile-exporters     (make-granny-subdirectory "export"))
(make-rad-compile rad-compile-test          (make-granny-subdirectory "test"))
(make-rad-compile rad-compile-compress      (make-granny-subdirectory "compress"))
(make-rad-compile rad-compile-oodle2        (make-granny-subdirectory "oodle2"))
(make-rad-compile rad-compile-failacy       (make-granny-subdirectory "failacy"))
(make-rad-compile rad-compile-failacy-and-run (make-granny-subdirectory "failacy") "t -r && ruby failacy.rb test_test.exe")
(make-rad-compile rad-compile-doc           (make-granny-subdirectory "doc"))
(make-rad-compile rad-compile-viewer        (make-granny-subdirectory "viewer"))
(make-rad-compile rad-compile-crapi         (make-rad-subdirectory    "utils/crapi"))
(make-rad-compile rad-compile-crapi_classic (make-rad-subdirectory    "utils/crapi_classic"))
(make-rad-compile rad-compile-daged         (make-granny-subdirectory "daged"))
(make-rad-compile rad-compile-statement     (make-granny-subdirectory "statement"))
(make-rad-compile rad-compile-ue3           (make-granny-subdirectory "ue3"))
(make-rad-compile rad-compile-spew          (make-granny-subdirectory "spew"))
(make-rad-compile rad-compile-compressor    (make-granny-subdirectory "utilities/ue3_explorer"))
(make-rad-compile rad-compile-units         (make-granny-subdirectory "units"))
(make-rad-compile rad-compile-units-and-run (make-granny-subdirectory "units") "t && units")
(make-rad-compile rad-compile-winrt         (make-granny-subdirectory "rt") "tmake cdep_winrt.mak")
(make-rad-compile rad-compile-ps3           (make-granny-subdirectory "rt") "tmake cdep_ps3.mak")
(make-rad-compile rad-compile-durango       (make-granny-subdirectory "rt") "tmake cdep_durango.mak")
(make-rad-compile rad-compile-orbis         (make-granny-subdirectory "rt") "tmake cdep_orbis.mak")
(make-rad-compile rad-compile-psp2          (make-granny-subdirectory "rt") "tmake cdep_psp2.mak")
(make-rad-compile rad-compile-n3ds          (make-granny-subdirectory "rt") "tmake cdep_n3ds.mak")
(make-rad-compile rad-compile-wiiu          (make-granny-subdirectory "rt") "tmake cdep_wiiu.mak")
(make-rad-compile rad-compile-xenon         (make-granny-subdirectory "rt") "tmake cdep_xenon.mak")
(make-rad-compile rad-compile-gstate        (make-granny-subdirectory "gstate") "c:/apps/MSVC_9/Common7/IDE/devenv.com gstate.vcproj /Build \"Debug|Win32\"")
(make-rad-compile rad-compile-preprocessor  (make-granny-subdirectory "preprocessor") "c:/apps/MSVC_9/Common7/IDE/devenv.com preprocessor.vcproj /Build Debug")

;; Work bindings...
(bind-rad-compile "<f6>"  rad-compile-rt)
(bind-rad-compile "<f7>"  rad-compile-exporters)
(bind-rad-compile "<f8>"  rad-compile-viewer)
(bind-rad-compile "<f9>"  rad-compile-winrt)
(bind-rad-compile "<f11>" rad-compile-gstate)
(bind-rad-compile "<f12>" rad-compile-statement)

(global-set-key (kbd "<f5>") (lambda () (interactive) (compile "cd ~/granny3/build; /opt/local/bin/ninja units")))

;; Fix stupid C-up/down behavior
(global-set-key   (kbd "C-<down>")  (lambda () (interactive) (forward-line 7)))
(global-set-key   (kbd "C-<up>")    (lambda () (interactive) (forward-line -7)))

;; Better error movement
(global-set-key   (kbd "M-`") 'first-error)
(global-set-key   (kbd "M-1") 'previous-error)
(global-set-key   (kbd "M-2") 'next-error)
